#!/usr/bin/python
import os
import sys

# add functions folders the the system path
# sys.path.append(os.getcwd()+'/functions')

# get true path
FUNC_PATH = sys.argv[0].replace('umi-gen.py', 'functions')
sys.path.append(FUNC_PATH)

# # import local modules
# from LaunchVC import *
# from ParseBED import *
# from AddNoiseToPileup import *
# from GenerateReferenceReads import *
# from AddNoiseToReads import *
# from AddVariantsToReads import *


from functions import *

from Config import *



Is_SNV = True if 'snv' in sys.argv else False

config = Config()


value = Start(FUNC_PATH)
tmp_name = value[0]  
startTime = value[1]  



if Is_SNV:
	from SNV import *
	SNV(config)
else:
	from CNV import *
	CNV(config) 




Exit(tmp_name, startTime)