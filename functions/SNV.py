
#!/usr/bin/python
import os
import sys

from functions import *

# import local modules
# from ParseBED import *
# from LaunchVC import *
# from ParseBED import *
# from pyfasta import Fasta
# from GenerateReferenceReads import *
# from AddNoiseToPileup import *
# from AddNoiseToReads import *
# from Output import *
# from Config import *
# from FilterPositions import *
# from EstimateQualityLoss import *
# from AddVariantsToReads import *
# from utils import RemoveExtraNoise



def SNV(config):
	
	# load and define variables from the config
	samples              = config['input']
	BED                  = config['bed']
	FASTA                = config['fasta']
	VARIANTS             = config["variants"]
	MIN_BASE_QUALITY     = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY  = int(config['min_mapping_quality'])
	MIN_READ_QUALITY     = int(config['min_read_quality'])
	MIN_VARIANT_UMI      = int(config['min_variant_umi'])
	STRAND_BIAS_METHOD   = str(config['strand_bias_method'])
	MAX_STRAND_BIAS      = float(config['max_strand_bias'])
	PILEUP               = config['pileup']
	MATRIX               = config['quality_matrix']
	REBUILD              = False if os.path.isfile(PILEUP) else True
	OUTPUT               = config['output']
	ALPHA                = float(config['alpha'])
	MAX_HP_LENGTH        = int(config['max_hp_length'])
	DEPTH                = int(config['depth'])
	AMP_FACTOR           = int(config['amp_factor'])
	UMI_LENGTH           = int(config['umi_length'])
	READ_LENGTH          = int(config['read_length'])
	MAX_NOISE_RATE       = float(config['max_noise_rate'])
	OUTPUT_NAME          = config['name']+"_snv_"+str(DEPTH)
	NAME                 = config['name']
	PLATFORM             = config['platform']
	VERSION              = config['version']
	LAST_UPDATE          = config['lastUpdate']

	try:
		os.mkdir(OUTPUT+"/"+OUTPUT_NAME)
	except:
		pass

	OUTPUT = OUTPUT+"/"+OUTPUT_NAME

	# print parameters in the console
	if len(samples) == 1:
		PrintTime("green", "\t\tINPUT file   : "+samples[0])
	else:
		PrintTime("green", "\t\t# SAMPLES    : "+str(len(samples)))

	PrintTime("green", "\t\tBED file     : "+BED)
	PrintTime("green", "\t\tFASTA file   : "+FASTA)

	if PILEUP != "None":	
		PrintTime("green", "\t\tPILEUP file  : "+PILEUP)

	if MATRIX != "None":	
		PrintTime("green", "\t\tMATRIX file  : "+MATRIX)

	PrintTime("green", "\t\tOutput       : "+OUTPUT+"/"+OUTPUT_NAME)

	if VARIANTS != "None":
		PrintTime("green", "\t\tVAR file     : "+VARIANTS)

	PrintTime("green", "\t\tmin_base_quality      : "+str(MIN_BASE_QUALITY))
	PrintTime("green", "\t\tmin_read_quality      : "+str(MIN_READ_QUALITY))
	PrintTime("green", "\t\tmin_mapping_quality   : "+str(MIN_MAPPING_QUALITY))
	PrintTime("green", "\t\tmin_variant_umi       : "+str(MIN_VARIANT_UMI))
	PrintTime("green", "\t\tstrand_bias_method    : "+str(STRAND_BIAS_METHOD))
	PrintTime("green", "\t\tmax_strand_bias       : "+str(MAX_STRAND_BIAS))
	PrintTime("green", "\t\tmax_hp_length         : "+str(MAX_HP_LENGTH))
	PrintTime("green", "\t\talpha                 : "+str(ALPHA))

	PrintTime("green", "\t\tmax_noise_rate        : "+str(MAX_NOISE_RATE))
	PrintTime("green", "\t\tamp_factor            : "+str(AMP_FACTOR))

	PrintTime("green", "\t\tdepth                 : "+str(DEPTH))
	PrintTime("green", "\t\tumi_length            : "+str(UMI_LENGTH))
	PrintTime("green", "\t\tread_length           : "+str(READ_LENGTH))

	PrintTime("green", "\t\tplatform              : "+str(PLATFORM)+"\n")

	PrintTime("green", "\t\tVERSION               : "+VERSION)
	

	PrintTime("console", "\tDone\n")



	variants = set([])
	pileup = ParseBED(BED)
	f = Fasta(FASTA)


	if PILEUP == "None":

		### create d for qscores at each position
		qScores = {}
		for chrom in pileup.keys():
			for pos in pileup[chrom].keys():
				try:
					qScores[chrom][pos] = []
				except:
					qScores[chrom] = {pos: []}


		for sample in samples:
			value = LaunchVC(pileup, sample, samples.index(sample)+1, config, "snv")
			pileup = value[0]

			for chrom in pileup.keys():
				for pos in pileup[chrom].keys():
					qScores[chrom][pos].append(pileup[chrom][pos]['qScore'])

			for chrom in value[1].keys():
				for pos in value[1][chrom].keys():
					chrom = chrom.split('|')[0]
					variants.add(chrom+"-"+str(pos))



		AddNoiseToPileup(pileup, variants, qScores)


		try:
			os.mkdir('pileups')
		except:
			pass
		
		with open("pileups/mini_snv.pileup", 'wb') as handle:
			msgpack.pack(pileup, handle, encoding="utf-8")

	else:

		print("\n")
		PrintTime('console', "\tLoading Pileup...")

		with open(PILEUP, 'rb') as handle:
			pileup = msgpack.unpack(handle, encoding="utf-8")

		PrintTime('console', "\tDone")




	if MATRIX == "None":

		qualityMatrix = EstimateQualityLoss(samples)

		with open(OUTPUT+"/quality.matrix", 'wb') as handle:
			msgpack.pack(qualityMatrix, handle, encoding="utf-8")

	else:

		print("\n")
		PrintTime('console', "\tLoading Quality Matrix...")

		with open(MATRIX, 'rb') as handle:
			qualityMatrix = msgpack.unpack(handle, encoding="utf-8")

		PrintTime('console', "\tDone")




	# remove extra variants / any variants that the variant caller may have not detected
	# reinitialize positions where ref allele is lower than 95%
	RemoveExtraNoise(pileup, MAX_NOISE_RATE)



	# get MAX_READ_LENGTH and use it in ParseRanges in order
	# to avoid having ranges with length > MAX_READ_LENGTH
	MAX_READ_LENGTH = max(list(qualityMatrix.keys()))

	ranges_d = ParseRanges(BED, READ_LENGTH, MAX_READ_LENGTH)


	if VARIANTS == "None":
		variants = {}
		addVariants = False

	else:

		print("\n")
		PrintTime('console', "\tAnalyzing Variants...")

		addVariants = True
		value = ParseVariantsFile(VARIANTS, ranges_d, DEPTH)
		variants = value[0]
		removed_1  = value[1]
		removed_2  = value[2]
		warning = value[3]

		if len(variants) > 0:
			for v in removed_1:
				PrintTime('warning', '\t\tWarning: the variant '+v+' was ignored (Not enough depth to add at this frequency)')
			for v in removed_2:
				PrintTime('warning', '\t\tWarning: the variant '+v+' was ignored (Another variant is to be added on the same amplicon)')
			if warning == 1 :
				PrintTime('warning', '\t\tWarning: 1 variant (with frequency lower than '+str(float(50.0/DEPTH))+') could present less than 5 UMI groups !')
			elif warning > 0:
				PrintTime('warning', '\t\tWarning: '+str(warning)+' variants (with frequencies <= '+str(float(50.0/DEPTH))+') could present less than 5 UMI groups !')


		else:
			addVariants = False

		PrintTime('console', "\tDone")





	


	value = GenerateReferenceReads(pileup, variants, ranges_d, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH)
	pileup = value[0]
	reads = value[1]
	usedUMIS = value[2]




	value = AddNoiseToReads(pileup, reads, ranges_d, BED, READ_LENGTH)
	pileup = value[0]
	reads = value[1]


	if addVariants:
		value = AddVariantsToReads(pileup, reads, variants, usedUMIS, BED, DEPTH, UMI_LENGTH, READ_LENGTH, AMP_FACTOR)
		reads = value[0]
		insertions = value[1]
		deletions = value[2]
	else:
		insertions = {}
		deletions = {}

	Output(reads, FASTA, OUTPUT, OUTPUT_NAME, insertions, deletions, NAME, PLATFORM, qualityMatrix)








