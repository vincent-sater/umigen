
### import functions from the script in the same dir
from utils import *


def PrintHelp(tool):

	tools = ["general", "snv", "cnv"]

	messages = []

	message = """
GENERAL DESCRIPTION
===============================================

This software's main purpose is to produce artificial UMI-tagged short reads that resemble the reads generated from a real 
NGS tumor sequencing. UMI-Gen starts by estimating the mean background error noise in the control samples provided by 
the user. Any variation detected in the control samples that has a frequency higher than a max_noise_rate threshold (0.05 
by default) is automatically removed. The error noise is estimated at each position and inserted in the produced reads. 
Finally, the user can provide a variants file in order to introduce single nucleotide variants (SNV) or copy number 
variants (CNV) in the final reads. A standalone tool is designed for each type of variants. UMI-Gen generates the R1 
FASTQ file, the R2 fastq file and both SAM and BAM alignment files.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Version: 1.1 
:Last update: 24-03-2022
:Tags: Genomics, UMI, Simulator, Reads, Variant Calling, CNV


There are 2 tools available in this software:

	-SNV
	-CNV


To get help on a specific tool, type:

	python umi-gen.py <tool>
	or 
	python umi-gen.py <tool> --help
	or 
	python umi-gen.py <tool> -h

To use a specific tool, type:

	python umi-gen.py <tool> [tool arguments]\n\n"""

	messages.append(message)
	




	message = """
TOOL DESCRIPTION : SNV
===============================================

This tool's main purpose is to produce artificial UMI-tagged short reads that resemble the reads generated from a real 
NGS tumor sequencing. UMI-Gen starts by estimating the mean background error noise in the control samples provided by 
the user. Any variation detected in the control samples that has a frequency higher than a max_noise_rate threshold (0.05 
by default) is automatically removed. The error noise is estimated at each position and inserted in the produced reads. 
Finally, the user can provide a variants file in order to introduce single nucleotide variants (SNV) in the final reads. 
UMI-Gen generates the R1 FASTQ file, the R2 fastq file and both SAM and BAM alignment files.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Version: 1.1 
:Last update: 24-03-2022
:Tags: Genomics, UMI, Simulator, Reads, Variant Calling, SNV


USAGE
===============================================

python umi-gen.py snv [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-bwa
-samtools
-Python modules:    
                    msgpack-python (version 0.5.6)
                    pysam
                    pyfasta
                    scipy.stats
                    statsmodels.stats.multitest


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	For example: /home/my_dir/my_subdir/hg19.fasta

-i, --input=LIST OF FILES (case sensitive)
	The input is a comma separated list of control BAM/SAM files 
	that will be used to estimate the background error rate of the
	sequencer. Using the full path to each file is recommended. 
	For example: /home/my_dir/my_subdir/ind1.sam,/home/my_dir/my_
	subdir/ind2.bam,/home/my_dir/my_subdir/ind3.sam

-b, --bed=FILE (case sensitive)
	The panel design file in a BED format.
	It is recommended that you enter the BED file full path
	For example: /home/my_dir/my_subdir/design.bed


OPTIONAL ARGUMENTS
===============================================

-p, --pileup=FILE (case sensitive)
	The PILEUP file for the control files previously dumped during
	an older analysis with the same BED and FASTA files. This 
	is not the pileup format generated by SAMtools. If specified,
	the tool will attempt to retrieve the counts of each position
	from the PILEUP file instead of rebuilding the pileup from 
	scratch, a process that is relatively fast and can save time.
	It is recommended that you enter the PILEUP file full path.
	For example: /home/my_dir/my_subdir/ind1-2-3-5.pileup

-m, --quality_matrix=FILE (case sensitive)
	The MATRIX file for the control files previously dumped during
	an older analysis with the same BED and FASTA files. If specified,
	the tool will attempt to retrieve the relative quality scores per 
	position in read instead of recalculating the matrix from scratch, 
	a process that is relatively fast and can save time.
	It is recommended that you enter the MATRIX file full path.
	For example: /home/my_dir/my_subdir/ind1-2-3-5.matrix

-o, --output=DIR (case sensitive)
	The output directory in which your files will be generated.
	It is recommended that you enter the output dir full path.
	For example: /home/my_dir/my_subdir/out_dir

-n, --name=STR
	This represents that you want to give to the generated sample.
	A new directory will be created with this name in the OUTPUT dir
	and all generated files will be in it.
	The value of this parameter defaults to 'sim'

-v, --variants=FILE (case sensitive)
	The variants file is a normal CSV file that can be used to 
	introduce known mutations in the final reads. This can be 
	specifically handy for variant calling testing purposes. The 
	file must have 2 columns and a header: variant | frequency. The 
	variant annotation must respect the HGVS-nomenclature and the
	second column must contain frequencies and not percentages.
	Substitutions, insertions and deletions are supported. Inserted 
	variants must be on different amplicons (or separated by at least
	1 * READ_LENGTH)
	It is recommended that you enter the variants file full path.
	For example: /home/my_dir/my_subdir/variants.csv

--min_base_quality=INT
	The minimum value for which a base with a certain quality is
	considered as "good". Only bases with a quality >= min_base_quality
	will be considered.
	The value of this parameter defaults to 10 

--min_read_quality=INT
	The minimum value for which a read with a certain mean quality is
	considered as "good". Only reads with a quality >= min_read_quality
	will be considered.
	The value of this parameter defaults to 20 

--min_mapping_quality=INT
	The minimum value for which a read with a certain mapping 
	quality is considered as "good". Only bases with a mapping
	quality >= min_mapping_quality will be considered.
	The value of this parameter defaults to 20

--min_variant_umi=INT
	The minimum count for a variant with a certain unique UMIs 
	count to be called. Only variants with a unique UMI count >= 
	min_variant_umi will be called.
	The value of this parameter defaults to 10

--alpha=INT/FLOAT
	This is the type I error probability known as alpha level.
	Positions with a p-value/q-value >= alpha will be considered as
	noisy and variants in these positions won't be called.
	The value of this parameter defaults to 0.05 (5%)

--strand_bias_method=STR
	This parameter lets you specifiy the method you want to use
	to calculate variant strand bias. The value can only be 
	"default" or "torrent_suite". Choosing any of the methods 
	can have minimal effect on the final variants called and the SB
	value differs: the torrent suite method generates SB values [0.5;1]
	but the default method generates SB values [0;+Inf].
	Obviously, this parameter defaults to the value "default"

--max_strand_bias=INT/FLOAT
	The minimum strand bias value for a variant with a certain  
	negative and positive coverage to be called. Only variants 
	with a strand bias <= max_strand_bias will be called.
	The value of this parameter defaults to 1.0 for the default
	method and to 0.743 for the torrent suite method
	
--max_hp_length=INT
	The maximum homopolymer length for a variant at a certain  
	position in an homopolymer region to be called. Only variants 
	with a homopolymer length <= max_hp_length will be called.
	The value of this parameter defaults to 7

-d, --depth=INT
	The wanted depth of the produced sample. The minimal depth is
	1000 and the maximum depth is fixed at 100 000. This should be 
	taken into account if you wish to introduce variants at very 
	low frequencies. The value of this parameter defaults to 1000

-u, --umi_length=INT
	The length of the generated UMI tags. The minimal UMI length is
	fixed at 6 and the maximal length is fixed at 15.
	The value of this parameter defaults to 12

--read_length=INT
	The length of the generated reads. The minimal read length is
	fixed at 80 and the maximal length is fixed at 150.
	The value of this parameter defaults to 110

--max_noise_rate=FLOAT
	This parameter represents the frequency above which the variants
	from the control samples are removed. It should be betweeen 0 and 1. 
	The value of this parameter defaults to 0.05

--amp_factor=FLOAT
	This parameter represents the average amplification factor of the 
	reads. At any position, depth = amp_factor * 2 * n_unique_umis.
	For the same depth, increasing this parameter will decrease the 
	number of unique UMI tags. The default value of this parameter is 
	calculated as to allow optimal performance and optimal results. 

--platform=STR
	This parameter corresponds to sequencing platform used to create 
	the sample. Accepted values are CAPILLARY, LS454, ILLUMINA, SOLID,
	HELICOS, IONTORRENT, ONT and PACBIO.
	The default value is ILLUMINA.


EXAMPLE
===============================================

python3 umi-gen.py snv -i /home/.../ind1.sam,/home/.../ind3.sam -b /home/.../design.bed -f /home/.../hg19.fa -p /home/.../test.pileup -m /home/.../test.matrix -o /home/.../out -n test -d 1000 -v /home/.../variants.csv -u 9 --read_length 115 --min_base_quality 15 --min_read_quality 25 --min_mapping_quality 40 --min_variant_umi 7 --strand_bias_method default --max_strand_bias 0.8 --alpha 0.01 --max_hp_length 5 --max_noise_rate 0.03 --amp_factor 35 --platform SOLID


OUTPUT
===============================================

1-  A PILEUP file (<INPUT_NAME>.pileup) corresponding to the entire pileup dumped.
    This file is produced in the pileups directory. If the pileups directory is not
    available, it will be created automatically. This file can be used to skip the
    pileup building and load it instead if the analysis was already done.  It is 
    not compatible with the CNV tool.

2-  A FASTQ_R1 file (<OUTPUT_NAME>_R1.fastq) containing all first-pair reads.

3-  A FASTQ_R2 file (<OUTPUT_NAME>_R2.fastq) containing all second-pair reads.

4-  A SAM file (<OUTPUT_NAME>.sam) containing all the aligned paired-end reads
    from the FASTQ_R1 and FASTQ_R2 files. The alignement is done using BWA algorithm.

5-  A BAM file (<OUTPUT_NAME>.bam) that is the compressed binary version of the 
    SAM file generated above.

6-  A BAM index file (<OUTPUT_NAME>.bam.bai) that is a companion file to your BAM file.
    It allows the program reading the BAM file to jump directly to specific parts of the
    file without reading through all of the sequences.

7-  A MATRIX file (quality.matrix) corresponding to the dictionnary containing quality 
    scores relative estimations per position in read in the control samples.
    This file can be used to skip the quality matrix calculation step and
    load it instead if the analysis was already done. This file is compatible
    with the CNV tool.\n\n"""


	messages.append(message)

	message = """
TOOL DESCRIPTION : CNV
===============================================

This tool's main purpose is to produce artificial UMI-tagged short reads that resemble the reads generated from a real 
NGS tumor sequencing. UMI-Gen starts by estimating the mean background error noise in the control samples provided by 
the user. Any variation detected in the control samples that has a frequency higher than a max_noise_rate threshold (0.05 
by default) is automatically removed. The error noise is estimated at each position and inserted in the produced reads. 
Finally, the user can provide a variants file in order to introduce copy number variants (CNV) in the final reads. 
UMI-Gen generates the R1 FASTQ file, the R2 fastq file and both SAM and BAM alignment files.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Version: 1.1 
:Last update: 24-03-2022
:Tags: Genomics, UMI, Simulator, Reads, Variant Calling, CNV


USAGE
===============================================

python umi-gen.py cnv [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-bwa
-samtools
-Python modules:   
                    msgpack-python (version 0.5.6)
                    pysam
                    pyfasta
                    scipy.stats
                    statsmodels.stats.multitest


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	For example: /home/my_dir/my_subdir/hg19.fasta

-i, --input=LIST OF FILES (case sensitive)
	The input is a comma separated list of control BAM/SAM files 
	that will be used to estimate the background error rate of the
	sequencer. Using the full path to each file is recommended. 
	For example: /home/my_dir/my_subdir/ind1.sam,/home/my_dir/my_
	subdir/ind2.bam,/home/my_dir/my_subdir/ind3.sam, 

-b, --bed=FILE (case sensitive)
	The panel design file in a BED format.
	It is recommended that you enter the BED file full path
	For example: /home/my_dir/my_subdir/design.bed


OPTIONAL ARGUMENTS
===============================================

-p, --pileup=FILE (case sensitive)
	The PILEUP file for the control files previously dumped during
	an older analysis with the same BED and FASTA files. This 
	is not the pileup format generated by SAMtools. If specified,
	the tool will attempt to retrieve the counts of each position
	from the PILEUP file instead of rebuilding the pileup from 
	scratch, a process that is relatively fast and can save time.
	It is recommended that you enter the PILEUP file full path.
	For example: /home/my_dir/my_subdir/ind1-2-3-5.pileup

-m, --quality_matrix=FILE (case sensitive)
	The MATRIX file for the control files previously dumped during
	an older analysis with the same BED and FASTA files. If specified,
	the tool will attempt to retrieve the relative quality scores per 
	position in read instead of recalculating the matrix from scratch, 
	a process that is relatively fast and can save time.
	It is recommended that you enter the MATRIX file full path.
	For example: /home/my_dir/my_subdir/ind1-2-3-5.matrix

-o, --output=DIR (case sensitive)
	The output directory in which your files will be generated.
	It is recommended that you enter the output dir full path.
	For example: /home/my_dir/my_subdir/out_dir

-n, --name=STR
	This represents that you want to give to the generated sample.
	A new directory will be created with this name in the OUTPUT dir
	and all generated files will be in it.
	The value of this parameter defaults to 'sim'

-v, --variants=FILE (case sensitive)
	The variants file is a normal CSV file that can be used to 
	introduce known CNV alterations in the final reads. This can be 
	specifically handy for CNV calling testing purposes. The file
	must have 5 columns and a header: chr | start | end | freq | type. 
	The fourth column must contain frequencies and not percentages.
	The fifth column must contain one of these 2 values : "amp" to 
	simulate an amplified region or "del" to simulate a deleted region.
	It is recommended that you enter the variants file full path.
	For example: /home/my_dir/my_subdir/variations.csv

--min_base_quality=INT
	The minimum value for which a base with a certain quality is
	considered as "good". Only bases with a quality >= min_base_quality
	will be considered.
	The value of this parameter defaults to 10 

--min_read_quality=INT
	The minimum value for which a read with a certain mean quality is
	considered as "good". Only reads with a quality >= min_read_quality
	will be considered.
	The value of this parameter defaults to 20 

--min_mapping_quality=INT
	The minimum value for which a read with a certain mapping 
	quality is considered as "good". Only bases with a mapping
	quality >= min_mapping_quality will be considered.
	The value of this parameter defaults to 20

--min_variant_umi=INT
	The minimum count for a variant with a certain unique UMIs 
	count to be called. Only variants with a unique UMI count >= 
	min_variant_umi will be called.
	The value of this parameter defaults to 5

--alpha=INT/FLOAT
	This is the type I error probability known as alpha level.
	Positions with a p-value/q-value >= alpha will be considered as
	noisy and variants in these positions won't be called.
	The value of this parameter defaults to 0.05 (5%)

--strand_bias_method=STR
	This parameter lets you specifiy the method you want to use
	to calculate variant strand bias. The value can only be 
	"default" or "torrent_suite". Choosing any of the methods 
	can have minimal effect on the final variants called and the SB
	value differs: the torrent suite method generates SB values [0.5;1]
	but the default method generates SB values [0;+Inf].
	Obviously, this parameter defaults to the value "default"

--max_strand_bias=INT/FLOAT
	The minimum strand bias value for a variant with a certain  
	negative and positive coverage to be called. Only variants 
	with a strand bias <= max_strand_bias will be called.
	The value of this parameter defaults to 1.0 for the default
	method and to 0.743 for the torrent suite method
	
--max_hp_length=INT
	The maximum homopolymer length for a variant at a certain  
	position in an homopolymer region to be called. Only variants 
	with a homopolymer length <= max_hp_length will be called.
	The value of this parameter defaults to 7

-d, --depth=INT
	The wanted depth of the produced sample. Depth can only take 100, 
	500, 1000, 2000, 3000, 5000, 10 000, 20000, 50000 or 100 000 as 
	values. This should be taken into account if you wish to introduce 
	variants at very low frequencies.
	The value of this parameter defaults to 1000

-u, --umi_length=INT
	The length of the generated UMI tags. The minimal UMI length is
	fixed at 6 and the maximal length is fixed at 15.
	The value of this parameter defaults to 12

--read_length=INT
	The length of the generated reads. The minimal read length is
	fixed at 80 and the maximal length is fixed at 150.
	The value of this parameter defaults to 110

--max_noise_rate=FLOAT
	This parameter represents the frequency above which the variants
	from the control samples are removed. It should be betweeen 0 and 1. 
	The value of this parameter defaults to 0.05

--amp_factor=FLOAT
	This parameter represents the average amplification factor of the 
	reads. At any position, depth = amp_factor * 2 * n_unique_umis.
	For the same depth, increasing this parameter will decrease the 
	number of unique UMI tags. The default value of this parameter is 
	calculated as to allow optimal performance and optimal results. 

--platform=STR
	This parameter corresponds to sequencing platform used to create 
	the sample. Accepted values are CAPILLARY, LS454, ILLUMINA, SOLID,
	HELICOS, IONTORRENT, ONT and PACBIO.
	The default value is ILLUMINA.


EXAMPLE
===============================================

python3 umi-gen.py cnv -i /home/.../ind1.sam,/home/.../ind3.sam -b /home/.../design.bed -f /home/.../hg19.fa -p /home/.../test.pileup -m /home/.../test.matrix -o /home/.../out -n test -d 1000 -v /home/.../variations.csv -u 9 --read_length 115 --min_base_quality 15 --min_read_quality 25 --min_mapping_quality 40 --min_variant_umi 7 --strand_bias_method default --max_strand_bias 0.8 --alpha 0.01 --max_hp_length 5 --max_noise_rate 0.03 --amp_factor 35 --platform PACBIO


OUTPUT
===============================================

1-  A PILEUP file (<INPUT_NAME>.pileup) corresponding to the entire pileup dumped.
    This file is produced in the pileups directory. If the pileups directory is not
    available, it will be created automatically. This file can be used to skip the
    pileup building and load it instead if the analysis was already done.  It is 
    not compatible with the SNV tool.

2-  A FASTQ_R1 file (<OUTPUT_NAME>_R1.fastq) containing all first-pair reads.

3-  A FASTQ_R2 file (<OUTPUT_NAME>_R2.fastq) containing all second-pair reads.

4-  A SAM file (<OUTPUT_NAME>.sam) containing all the aligned paired-end reads
    from the FASTQ_R1 and FASTQ_R2 files. The alignement is done using BWA algorithm.

5-  A BAM file (<OUTPUT_NAME>.bam) that is the compressed binary version of the 
    SAM file generated above.

6-  A BAM index file (<OUTPUT_NAME>.bam.bai) that is a companion file to your BAM file.
    It allows the program reading the BAM file to jump directly to specific parts of the
    file without reading through all of the sequences.

7-  A MATRIX file (quality.matrix) corresponding to the dictionnary containing quality 
    scores relative estimations per position in read in the control samples.
    This file can be used to skip the quality matrix calculation step and
    load it instead if the analysis was already done. This file is compatible
    with the SNV tool.\n\n"""



	messages.append(message)




	PrintProgramName()
	print(messages[tools.index(tool)])

	exit()
