
# THIS FILE CONTAINS GENERAL FUNCTIONS THAT ARE NEEDED TO 
# SUCCESSFULLY CALL THE VARIANTS IN THE BAM/SAM FILE 

import os
import sys
import math
import time
import pysam
import datetime
import random
import re
from collections import OrderedDict
from collections import defaultdict

# a class to define colors for each scenario
class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

# a function to print status messages + the time 
# + a specific color for each status
def PrintTime(nature, message, *args):
		
	if nature == "console":
		c_start = bcolors.BOLD+bcolors.OKBLUE
		c_end = bcolors.ENDC
	elif nature == "warning":
		c_start = bcolors.BOLD+bcolors.WARNING
		c_end = bcolors.ENDC
	elif nature == "error":
		c_start = bcolors.BOLD+bcolors.FAIL
		c_end = bcolors.ENDC
	elif nature == "red":
		c_start = bcolors.BOLD+bcolors.FAIL
		c_end = bcolors.ENDC
	elif nature == "green":
		c_start = bcolors.BOLD+bcolors.OKGREEN
		c_end = bcolors.ENDC

	if args:
		message = message % args
		
	print("[ " + time.strftime('%X') + " ]\t"+c_start+ message + c_end)
	sys.stdout.flush()
	sys.stderr.flush()



# function that calculates and displays progress efficiently
def PrintProgress(currentRead, totalReads, lastProgress,):
	progress = int((currentRead/totalReads*100))
	
	if int(progress) > int(lastProgress):
		### write progress in status file
		sys.stdout.write("\r[ " + time.strftime('%X') + " ]\t\t\tWorking "+str(int(progress))+" %")
		sys.stdout.flush()
		# time.sleep(0.001)

	return progress


# function to retieve intial RAM Usage and the exact launching time of the script 
def Start_dev(path):

	startTime = datetime.datetime.now()
	tmp_name = '.'+str(int(time.time()))
	os.mkdir(tmp_name)
	os.system('python '+path+'/RAM.py '+tmp_name+' &')
	return [tmp_name, startTime]



def Exit_dev(tmp_name, startTime):
	
	endTime = datetime.datetime.now()
	totalTime = endTime - startTime

	usedRAM = 0.0
	for x in os.listdir(tmp_name):
		if ".ram" in x:
			usedRAM = float(x[1:-4])
			break

	stop = open(str(tmp_name)+'/.done', 'w')
	stop.close()

	PrintTime('console', "\tDone")

	print('\r\n')
	message = "\tElapsed time :  "+str(totalTime)
	PrintTime("console", message)
	
	try:
		import psutil #dev only

		message = "\tRAM USAGE    :  "+str(round(usedRAM, 2))+" GB"+"\n\n"
		PrintTime("console", message)

		time.sleep(1)
		for x in os.listdir(tmp_name):
			os.remove(tmp_name+"/"+x)
		
		os.rmdir(tmp_name)	
	
	except:
		print("\n")

	exit()



def Start(path):

	startTime = datetime.datetime.now()
	tmp_name = '.'+str(int(time.time()))
	return [tmp_name, startTime]



def Exit(tmp_name, startTime):
	
	endTime = datetime.datetime.now()
	totalTime = endTime - startTime

	message = "\t\tElapsed time :  "+str(totalTime)
	PrintTime("green", message)
	PrintTime("console", "\tEND")
	print("\n")


	exit()


# this function will go through the pileup dictionnary and replace sets with lists
# this is because to successfully dump the pileup as a msppack object, sets are not
# supported and should be replaced with basic lists
def RemoveSets(d):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			d[chrom][position]['A'][2] = list(composition['A'][2])
			d[chrom][position]['C'][2] = list(composition['C'][2])
			d[chrom][position]['G'][2] = list(composition['G'][2])
			d[chrom][position]['T'][2] = list(composition['T'][2])
			
			for key, value in composition['in'].items():
				d[chrom][position]['in'][key][2] = list(value[2])
			for key, value in composition['del'].items():
				d[chrom][position]['del'][key][2] = list(value[2])

	return d





# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate the 
# depth. the depth = nA+ + nA- + nC+ + nC- + nG+ + nG- + nT+ + nT- + nDel+ + nDel- 
# insertion counts are not taken into account in depth calculation
def AddDepths(d):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			depth =  composition['A'][0]+composition['A'][1]
			depth += composition['C'][0]+composition['C'][1]
			depth += composition['G'][0]+composition['G'][1]
			depth += composition['T'][0]+composition['T'][1]

			n_ins = 0
			for value in composition['in'].values():
				n_ins += value[0]+value[1]
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# insertions shouldn't be taken into account when calculating depth ??
			depth += n_ins
			depth += n_del

			d[chrom][position]['depth'] = depth

	return d



# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate use the
# position depth and the total base quality scores to estimate 
# the noise level at the position
def EstimateNoise(d):
	for chrom, infos in d.items():
		for position, composition in infos.items():
			# get total base quality scores at this position
			qScore = d[chrom][position]['base_error_probability']
			
			# get depth
			depth = d[chrom][position]['depth']
			
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# substract deletion from depth because deletions are not taken into account
			# in the total qscore since deletions don't have qscore
			depth -= n_del
			# divide by the depth to get mean qscore of the position
			mean_qscore = math.floor(float(qScore)/depth) if depth > 0 else 0
			d[chrom][position]['qScore'] = mean_qscore

			# qScore = -10.log(base_error_probability)
			# use this formula to calculate base_error probability
			base_error_probability = 10**(float(mean_qscore)/-10)
			# add the estimated base_error_probability to the position in the pileup
			d[chrom][position]['base_error_probability'] = round(base_error_probability, 6)

	return d








# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate use the
# position depth and the total base quality scores to estimate 
# the noise level at the position
def CopyNoise(pileup, pileup_mini):
	for chrom, infos in pileup_mini.items():
		for position, composition in infos.items():

			pileup_mini[chrom][position]['qScore'] = pileup[chrom][position]['qScore']

	return pileup_mini







# this function takes the pileup dictionnary as an argument and the refernce genome
# for each chrom and at each position, it will retrieve the reference base in the given
# genome and add it to the pileup dictionnary
def AddRefBases(d, f):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			ref = f[chrom][position-1]
			d[chrom][position]['ref'] = ref.upper()

	return d




# this function generate a random UMI sequence given a specific length
def GenerateUMI(length):
	alphabet = "ACGT"
	return ''.join(random.choice(alphabet) for _ in range(length))



def ParseRangesFromReads(indexes):
	ranges = {}

	for x in indexes:
		x = x.split('-')
		chrom = x[0]
		start = int(x[1])
		end   = int(x[2])

		try:
			ranges[chrom].append([start, end])
		except:
			ranges[chrom] = [[start, end]]

	return ranges


# this fucntion parses the variants file and return a dictionnary
# with every variant infos (SNV)
def ParseVariantsFile(f, ranges_d, depth):
	variants = {}
	removed = []
	all_variants = {}
	warning = 0

	for line in open(f):
		if "chr" in line:
			line = line.split(",")
			matchElement = re.match('(chr[A-Z0-9]*:[0-9]*)([A-Z]*)>([A-Z]*).*', line[0])
			variant = matchElement.group(1)
			ref = matchElement.group(2)
			alt = matchElement.group(3)
			freq = float(line[1])
			all_variants[variant] = variant+ref+">"+alt

			if freq * depth < 10:
				removed.append(line[0])
			else:
				if freq <= float(50.0/depth):
					warning += 1
				if len(ref) > len(alt):
					variants[variant] = [ref, freq, "del"]
				elif len(ref) < len(alt):
					variants[variant] = [alt, freq, "in"]
				else:
					variants[variant] = [alt, freq, "sub"]


	mutated_amplicons = []
	toRemove = []

	for variant in variants:
		chrom = variant.split(":")[0]
		pos   = int(variant.split(":")[1])
		ranges = ranges_d[chrom]
		
		i = 0
		for x in ranges:
			if pos in range(x[0], x[1]+1):
				if chrom+"-"+str(i) not in mutated_amplicons:
					mutated_amplicons.append(chrom+"-"+str(i))
				else:
					toRemove.append(all_variants[variant])
			i += 1


	for x in toRemove:
		matchElement = re.match('(chr[A-Z0-9]*:[0-9]*)([A-Z]*)>([A-Z]*).*', x)
		variant = matchElement.group(1)
		del variants[variant]


	return [variants, removed, toRemove, warning]





# this fucntion parses the variations file and return a dictionnary
# with every variation infos (CNV)
def ParseVariationsFile(f):
	variations = {}
	for line in open(f):
		if "type" not in line:
			line = line.split(",")
			chrom = line[0]
			start = line[1]
			end = line[2]
			freq = float(line[3])
			typee = line[4][:-1]

			try:
				variations[chrom][start+"-"+end] = {'type': typee, 'freq': freq}
			except:
				variations[chrom] = {start+"-"+end : {'type': typee, 'freq': freq}}


	return variations



# this function parses the bed file and creates ranges of the covered positions.
# The ranges have the length of the reads so in fact, they correspond to the reads.
def ParseRanges(BED, READ_LENGTH, MAX_READ_LENGTH):

	ranges_d = {}

	for line in open(BED):

		# split the line using tab as delimiter
		line = line.split('\t')

		# first element is the chromosome
		chrom = line[0]

		# second element is the start of the region
		# third element is the end of the region
		# create a list containing start and end 
		# sort to ensure that start < end
		limits = [int(line[1]), int(line[2])]
		limits.sort()

		try:
			ranges_d[chrom].append(limits)
		except:
			ranges_d[chrom] = [limits]







	ranges_f = {}

	for chrom in ranges_d.keys():
		ranges_f[chrom] = []



	for chrom in ranges_d.keys():

		c = 0
		nRanges = len(ranges_d[chrom])
		newRange = []
		newRange2 = []

		if nRanges == 1:

			newRange = ranges_d[chrom][0]
			newRange = list(range(newRange[0], newRange[1]+1))

			nReads = int(round(len(newRange)/float(READ_LENGTH), 0)) 
			nReads = 1 if nReads == 0 else nReads

			lenRange = math.ceil(len(newRange)/nReads)-1
			
			if nReads == 1:
				tmp = [[newRange[0], newRange[-1]]]
			else:
				tmp = [[newRange[0], newRange[0]+lenRange-1]]
				for i in range(1, nReads-1):
					tmp.append([newRange[i]+((lenRange-1)*i), newRange[i]+((lenRange-1)*(i+1))])
				tmp.append([tmp[-1][1]+1, newRange[-1]])

			# print("tmp 1:   "+str(tmp))

			final = []

			for x in tmp:
				if x[1]-x[0] >= MAX_READ_LENGTH:
					
					newRange = list(range(x[0], x[1]+1))

					nReads = math.ceil(len(newRange)/float(MAX_READ_LENGTH)) 

					lenRange = math.ceil(len(newRange)/nReads)-1
					
					if nReads == 1:
						final.append([newRange[0], newRange[-1]])
					else:
						final.append([newRange[0], newRange[0]+lenRange-1])
						for i in range(1, nReads-1):
							final.append([newRange[i]+((lenRange-1)*i), newRange[i]+((lenRange-1)*(i+1))])
						final.append([final[-1][1]+1, newRange[-1]])

				else:
					final.append(x)
			
			# print("final 1: "+str(final))
			ranges_f[chrom].extend(final) 


		else:

			while c < nRanges-1:


				ranges1 = ranges_d[chrom][c]
				ranges2 = ranges_d[chrom][c+1]

				# print('-------------------------------')
				# print(ranges1)
				# print(ranges2)


				newRange.append(ranges1[0])


				if ranges2[0] > ranges1[1]:
					newRange.append(ranges1[1])

					if c + 1 == nRanges - 1:
						newRange2 = ranges2


				while ranges2[0] <= ranges1[1]:

					newRange.append(ranges2[1])

					c += 1
					if c == nRanges - 1:
						break
					else:
						ranges1 = ranges_d[chrom][c]
						ranges2 = ranges_d[chrom][c+1]




				if len(newRange) > 2:
					newRange = list(range(min(newRange), max(newRange)+1))
				else:
					newRange = list(range(newRange[0], newRange[1]+1))

				nReads = int(round(len(newRange)/float(READ_LENGTH), 0)) 
				nReads = 1 if nReads == 0 else nReads

				lenRange = math.ceil(len(newRange)/nReads)-1
				# print(newRange, len(newRange)/float(READ_LENGTH), nReads, lenRange)
				
				if nReads == 1:
					tmp = [[newRange[0], newRange[-1]]]
				else:
					tmp = [[newRange[0], newRange[0]+lenRange-1]]
					for i in range(1, nReads-1):
						tmp.append([newRange[i]+((lenRange-1)*i), newRange[i]+((lenRange-1)*(i+1))])
					tmp.append([tmp[-1][1]+1, newRange[-1]])

				# print("\ntmp 2:   "+str(tmp))
				final = []


				for x in tmp:

					if x[1]-x[0] >= MAX_READ_LENGTH:
						
						newRange = list(range(x[0], x[1]+1))

						nReads = math.ceil(len(newRange)/float(MAX_READ_LENGTH)) 

						lenRange = math.ceil(len(newRange)/nReads)-1
						
						if nReads == 1:
							final.append([newRange[0], newRange[-1]])
						else:
							final.append([newRange[0], newRange[0]+lenRange-1])
							for i in range(1, nReads-1):
								final.append([newRange[i]+((lenRange-1)*i), newRange[i]+((lenRange-1)*(i+1))])
							final.append([final[-1][1]+1, newRange[-1]])

					else:
						final.append(x)

				
				# print("final 2: "+str(final))
				ranges_f[chrom].extend(final) 


				c += 1

				newRange = []


			## if last line for a chrom is distinct of the ranges before it

			if len(newRange2) > 0:

				newRange = newRange2


				if len(newRange) > 2:
					newRange = list(range(min(newRange), max(newRange)+1))
				else:
					newRange = list(range(newRange[0], newRange[1]+1))

				nReads = int(round(len(newRange)/float(READ_LENGTH), 0)) 
				nReads = 1 if nReads == 0 else nReads

				lenRange = math.ceil(len(newRange)/nReads)-1
				# print(newRange, len(newRange)/float(READ_LENGTH), nReads, lenRange)
				
				if nReads == 1:
					tmp = [[newRange[0], newRange[-1]]]
				else:
					tmp = [[newRange[0], newRange[0]+lenRange-1]]
					for i in range(1, nReads-1):
						tmp.append([newRange[i]+((lenRange-1)*i), newRange[i]+((lenRange-1)*(i+1))])
					tmp.append([tmp[-1][1]+1, newRange[-1]])

				# print("\ntmp 3:   "+str(tmp))

				final = []

				for x in tmp:
					if x[1]-x[0] >= MAX_READ_LENGTH:
						
						newRange = list(range(x[0], x[1]+1))

						nReads = math.ceil(len(newRange)/float(MAX_READ_LENGTH)) 

						lenRange = math.ceil(len(newRange)/nReads)-1
						
						if nReads == 1:
							final.append([newRange[0], newRange[-1]])
						else:
							final.append([newRange[0], newRange[0]+lenRange-1])
							for i in range(1, nReads-1):
								final.append([newRange[i]+((lenRange-1)*i), newRange[i]+((lenRange-1)*(i+1))])
							final.append([final[-1][1]+1, newRange[-1]])

					else:
						final.append(x)
				
				# print("final 3: "+str(final))
				ranges_f[chrom].extend(final) 



	return ranges_f



# this function returns the reverse complement of a sequence
def ReverseComplement(read):
	read = read.upper()
	rc = ""
	complement = {"A": "T", "C": "G", "G": "C", "T": "A"}
	for base in read[::-1]:
		rc += complement[base]

	return rc


# this function takes 3 arguments : the quality string of the read, 
# the quals chracters string and the minimum read quality
# if the mean quality of the read is < minimum read quality, the
# function will return False (meaning the read is invalid) 
def QualIsValid(qual, quals, min_read_qual):
	total = 0
	l_read = len(qual)
	threshold = min_read_qual*l_read

	step = 0
	for q in qual:
		total += quals[q]
		step += 1
		if step == 10:
			if total >= threshold:
				return True
			step=0

	return False



# this function will check if the read is valid
# a valid read must not be an orphan, must be aligned to chromosome
# at a certain position, must have an UMI of length 12 (customizable),
# must have a read quality > min_read_quality and a mapping quality 
# different than 255. if all these conditions are met, the read is valid
# and the function returns True
def ReadIsValid(flag, chrom, pos, umi, cigar, mapq, min_mapping_qual, qual, quals, min_read_qual):

	orphan = not bool(int("{0:b}".format(int(flag))[-2]))

	# test if orphan
	if orphan:
		return False


	try:
		# test chromosome format
		test = chrom.index('chr')

		# test pos format
		test = 25/int(pos)

		# test umi length
		lUMI = len(umi)
		test = 25/(lUMI-1)
		test = 25/(lUMI-2)
		test = "xx12xx".index(str(len(umi)))
		
		# test cigar format
		test = 25/(1-len(cigar))

		# test read quality
		if not QualIsValid(qual, quals, min_read_qual):
			return False

		# test mapping quality
		if mapq == 255 or mapq < min_mapping_qual:
			return False


		return True

	except:

		return False


# this function will calculate the total number of keys in the given dictionnary
# and return it as a value
def GetPileupLength(pileup):
	total = 0
	
	for chrom , infos in pileup.items():
		for position, composition in infos.items():
			total += 1

	return total


# this function will calculate the total number of variants in the pileup dictionnary
# and return it as a value
def GetTotalVariants(pileup):
	total = 0
	
	for chrom , infos in pileup.items():
		for position, composition in infos.items():
			total += 1
			if composition["alt2"] != None:
				total += 1
			if composition["alt3"] != None:
				total += 1

	return total


# 
# 
def GetTotalPos1(d):
	total = 0

	for chrom, ranges in d.items():
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]
			for pos in range(start, end+1):
				total += 1

	return total


#
#
def GetTotalPos2(d):
	total = 0

	for x in d.keys():
		for read_id, read in d[x].items():
			total += 1

	return total



# this function will return the range in which a position occurs
def GetRange(ranges, position):
	for aRange in ranges:
		if position in range(aRange[0], aRange[1]+1):
			return aRange


# this function will return the number of lines in a file
def GetTotalLines(filename):
	with open(filename) as f:
		for i, l in enumerate(f):
			pass
	return i + 1


# this function takes the pileup dictionnary as an argument and the refernce genome
# for each chrom and at each position, it will calculate the length of the homopolymer 
# and add it to the pileup dictionnary
def AddHomoPolymers(d, f):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			ref = f[chrom][position-1]

			hp = 1
			# check hp left - max - 20
			for i in range(2, 22):
				x = f[chrom][position-i]
				if x == ref:
					hp += 1
				else:
					break

			# check hp right - max - 20
			for i in range(0, 20):
				x = f[chrom][position+i]
				if x == ref:
					hp += 1
				else:
					break


			d[chrom][position]['HP'] = hp

	return d



# this function takes the final variants dictionnary as an argument
# it will sort the dictionnary by chromosome first, and by position
# second and return the sorted dictionanry.
def SortVariants(finalVariants):

	tmp = OrderedDict()

	sorted_chroms = sorted(finalVariants.keys())

	chrom_strings = []
	tmp_sorted_chroms = []
	final_sorted_chroms = []
	seen = {}
	
	for sorted_chrom in sorted_chroms:
		try:
			tmp_sorted_chroms.append(int(float(sorted_chrom.replace('chr', '').replace('|v', '.1'))))
		except:
			chrom_strings.append(sorted_chrom)
			
	tmp_sorted_chroms = sorted(tmp_sorted_chroms)
	
	for x in tmp_sorted_chroms:
		if x not in seen:
			final_sorted_chroms.append('chr'+str(x)+"|v")
			seen[x] = 2
		else:
			y = 'chr'+str(x)+"|v"+str(seen[x])
			if y in finalVariants.keys():
				final_sorted_chroms.append('chr'+str(x)+"|v"+str(seen[x]))
				seen[x] += 1
			else:
				seen[x] += 1
				final_sorted_chroms.append('chr'+str(x)+"|v"+str(seen[x]))


	for x in final_sorted_chroms:
		tmp[x] = OrderedDict()

	for x in chrom_strings:
		tmp[x] = OrderedDict()


	for x in tmp.keys():
		subdict = finalVariants[x]
		positions = subdict.keys()
		sorted_positions = sorted(positions)

		for sorted_pos in sorted_positions:
			tmp[x][sorted_pos] = finalVariants[x][sorted_pos]


	return tmp



# this function will return a list with elements that 
# only occured once
def GetSingletons(l):
	d = defaultdict(int)
	for x in l: d[x] += 1
	k = [x for x in l if d[x] == 1]
	return k

# this function will return a list with elements that 
# occured more than once
def RemoveSingletons(l):
	d = defaultdict(int)
	for x in l: d[x] += 1
	k = [x for x in l if d[x] > 1]
	return k


# this function will call pysam to convert the BAM file into a SAM file
def BAMtoSAM(bam):
	sam = bam.replace('.bam', '.sam')
	samFile = open(sam, "w")
	samFile.close()
	pysam.view('-o', sam, bam, save_stdout=sam)
	return sam



# this function will give a variant a confidence level based
# on its q-value, alt_concordant_umi, strand bias and hp length.
def ComputeConfidence(qval, alt_discordant, alt_concordant, hp, Cp, Cm, Vp, Vm):

	sb = CalculateStrandBias(Cp, Cm, Vp, Vm, "default")
	confs = ['low', 'average', 'high', 'strong', 'certain']
	levels = []

	# check q-value
	if qval == 0:
		levels.append(5)
	elif qval < 0.00005:
		levels.append(4)
	elif qval < 0.0005:
		levels.append(3)
	elif qval < 0.005:
		levels.append(2)
	elif qval < 0.01:
		levels.append(1)
	else:
		levels.append(0)



	# check disordant/concordant ratio
	if alt_discordant == 0:
		levels.append(5)
	else:
		if alt_concordant > 30:
			levels.append(5)
		elif alt_concordant > 20:
			levels.append(4)
		elif alt_concordant > 15:
			levels.append(3)
		elif alt_concordant > 10:
			levels.append(2)
		else:
			levels.append(1)


	# check SB
	if sb < 0.5:
		levels.append(5)
	elif sb < 0.60:
		levels.append(4)
	elif sb < 0.70:
		levels.append(3)
	elif sb < 0.80:
		levels.append(2)
	elif sb < 0.90:
		levels.append(1)
	else:
		levels.append(0)

	
	# check HP
	if hp < 3:
		levels.append(5)
	elif hp == 3:
		levels.append(4)
	elif hp == 4:
		levels.append(3)
	elif hp == 5:
		levels.append(2)
	elif hp == 6:
		levels.append(1)
	else:
		levels.append(0)

	conf = int(math.floor(float(sum(levels))/len(levels)))
	return [conf, confs[conf-1]]



# this function will calculate the SB of the variant depending on the method the user selected
def CalculateStrandBias(Cp, Cm, Vp, Vm, method):
	if method == "default":
		return abs(((float(Vp)/(Cp+Vp))-(float(Vm)/(Cm+Vm)))/((float(Vp+Vm))/(Cp+Vp+Cm+Vm)))
	else:
		return float(max([Vp*Cm,Vm*Cp]) / (Vp*Cm + Vm*Cp))



def PrintProgramName():
	print(
"""
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
|||||||||||||||| ||||||| ||||  ||||||  ||||       ||||||||||||||||    ||||||        ||||  ||||| ||||||||||||||||
|||||||||||||||| ||||||| |||| | |||| | ||||||| |||||||||||||||||| |||| ||||| |||||||||||   |||| ||||||||||||||||
|||||||||||||||| ||||||| |||| || || || ||||||| ||||||||||||||||| ||||||||||| ||||||||||| |  ||| ||||||||||||||||
|||||||||||||||| ||||||| |||| |||  ||| ||||||| |||||||      |||| |||||||||||        |||| ||  || ||||||||||||||||
|||||||||||||||| ||||||| |||| |||||||| ||||||| ||||||||||||||||| ||||   |||| ||||||||||| |||  | ||||||||||||||||
||||||||||||||||  |||||  |||| |||||||| ||||||| |||||||||||||||||| |||| ||||| ||||||||||| ||||   ||||||||||||||||
|||||||||||||||||       ||||| |||||||| ||||       ||||||||||||||||    ||||||        |||| |||||  ||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||V.S|||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
""")











# this function will return a dictionnary with all UMIS and their indexes
def GetUMIS(SAM):

	value = set()

	for line in open(SAM):

		line = line.split('\t')
		
		try:
			value.add(line[0].split(':')[6].split('_')[1])

		except:
			continue

	
	c = 0
	final = {}

	for el in value:
		final[el] = c
		c+=1

	return final





def median(data):
    data.sort()
    mid = len(data) // 2
    return (data[mid] + data[~mid]) / 2






def InsertVariants100VAF(pileup, insert):

	forcedVariants = [
		["1", 2489780, "A", "T"], ["1", 2489799, "G", "C"], ["1", 2489823, "G", "T"], ["1", 2489835, "T", "A"],
		["1", 85733398, "G", "T"], ["1", 85733415, "C", "G"], ["1", 85733433, "G", "A"],
		["1", 203275124, "C", "G"], ["1", 203275179, "C", "A"],
		["2", 136875055, "C", "T"], ["2", 136875077, "A", "G"],
		["6", 26156549, "C", "A"], ["6", 26156556, "T", "G"], ["6", 26156572, "G", "C"], ["6", 26156600, "G", "T"],
		["6", 91005002, "C", "A"], ["6", 91005070, "C", "T"],
		["11", 111249119, "T", "G"], ["11", 111249144, "T", "C"], ["11", 111249160, "T", "A"],
		["16", 27356189, "G", "C"], ["16", 27356230, "T", "A"],
	]

	for fVar in forcedVariants:
		chrom = "chr"+fVar[0] 
		pos = fVar[1]
		ref = fVar[2]
		alt = fVar[3]

		# print(chrom+":"+str(pos)+ref+">"+alt)
		
		if insert:
			pileup[chrom][pos]['ref'] = alt
			pileup[chrom][pos][ref], pileup[chrom][pos][alt] = pileup[chrom][pos][alt], pileup[chrom][pos][ref]

		# in case the files produced with 100% VAF are to be concatenated with other files, the noise of the mutation in the other files must be eliminated.
		# otherwise, the final VAF of the variant will be higher than intended 
		# so we set insert=False when producing the base files => so the variant in the base files have 0 noise
		# we set insert=true in the files containing 100%VAF variants
		else:
			pileup[chrom][pos][alt] = [0.0, 0.0, []]


	return(pileup)







def RemoveExtraNoise(pileup, noiseMaxVAF):
	# a variant calling process is used to remove vriants from the control files
	# however, variants with VAF lower than 10% may persist or with VAF == 100%
	# this function will make sure to remove all the variants with VAF > 5%
	# in fact, positions having a total noise higher than noiseMaxVAF are reinitialized
	# meaning that no noise is inserted and that the ref allele wil  be represented at 100%
	alphabet = ['A', 'C', 'G', 'T']
	for chrom in pileup:
		for position, composition in pileup[chrom].items():
			g = pileup[chrom][position][pileup[chrom][position]['ref']][0]+pileup[chrom][position][pileup[chrom][position]['ref']][1]
			refObs = pileup[chrom][position][pileup[chrom][position]['ref']][0]+pileup[chrom][position][pileup[chrom][position]['ref']][1]
			if pileup[chrom][position][pileup[chrom][position]['ref']][1] > 0:
				if refObs < (1-noiseMaxVAF):
					# print(chrom+":"+str(position)+" => "+str(refObs))
					refRatio = pileup[chrom][position][pileup[chrom][position]['ref']][0]/pileup[chrom][position][pileup[chrom][position]['ref']][1]
					newFwdRatio = refRatio/(1+refRatio)
					newRevRatio = 1-newFwdRatio
					pileup[chrom][position][pileup[chrom][position]['ref']][0] = newFwdRatio
					pileup[chrom][position][pileup[chrom][position]['ref']][1] = newRevRatio
					notRef = list(set(alphabet)^set([pileup[chrom][position]['ref']]))
					for x in notRef:
						pileup[chrom][position][x][0] = 0.0
						pileup[chrom][position][x][1] = 0.0
			else:
				newFwdRatio = 0.5
				newRevRatio = 0.5
				pileup[chrom][position][pileup[chrom][position]['ref']][0] = newFwdRatio
				pileup[chrom][position][pileup[chrom][position]['ref']][1] = newRevRatio
				notRef = list(set(alphabet)^set([pileup[chrom][position]['ref']]))
				for x in notRef:
					pileup[chrom][position][x][0] = 0.0
					pileup[chrom][position][x][1] = 0.0

	return pileup
