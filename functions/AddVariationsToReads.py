
from utils import *
from collections import OrderedDict
import numpy
import random

def AddVariationsToReads(pileup, variations, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH):
	
	ranges_d = ParseRanges(BED, READ_LENGTH)


	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[i] = quals_str[i]
		i += 1



	reads = {"R1": OrderedDict(), "R2": OrderedDict()}


	read_counter = 0 
	usedUMIS = set([])

	print("\n")
	PrintTime('console', '\tAdding Variants...')

	for chrom, ranges in ranges_d.items():
		
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]

			try:
				test = reads['R1'][chrom+"-"+str(start)+"-"+str(end)]
			except:
				reads['R1'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()
				reads['R2'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()


			read = ""
			read_qual = ""

			for pos in range(start, end+1):

				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				pileup[chrom][pos]['A'][0] = round(pileup[chrom][pos]['A'][0]*DEPTH, 0)
				pileup[chrom][pos]['A'][1] = round(pileup[chrom][pos]['A'][1]*DEPTH, 0)

				pileup[chrom][pos]['C'][0] = round(pileup[chrom][pos]['C'][0]*DEPTH, 0)
				pileup[chrom][pos]['C'][1] = round(pileup[chrom][pos]['C'][1]*DEPTH, 0)

				pileup[chrom][pos]['G'][0] = round(pileup[chrom][pos]['G'][0]*DEPTH, 0)
				pileup[chrom][pos]['G'][1] = round(pileup[chrom][pos]['G'][1]*DEPTH, 0)

				pileup[chrom][pos]['T'][0] = round(pileup[chrom][pos]['T'][0]*DEPTH, 0)
				pileup[chrom][pos]['T'][1] = round(pileup[chrom][pos]['T'][1]*DEPTH, 0)



				n_umis   = 0
				
				### adapt amp factor and unique umis count depending on 
				### the variant frequency if position if mutated
				if chrom+":"+str(pos) in variations.keys():
					var_freq = variations[chrom+":"+str(pos)][1]
					var_reads = var_freq * DEPTH
					VAR_AMP_FACTOR = var_reads / 10


					if VAR_AMP_FACTOR > AMP_FACTOR:
						POS_AMP_FACTOR = AMP_FACTOR 
					else:
						POS_AMP_FACTOR = VAR_AMP_FACTOR

				else:
					POS_AMP_FACTOR = AMP_FACTOR


				# add variability so not all positions have exact AMP FACTOR
				# mean = POS_AMP_FACTOR
				# min = 0.85 * POS_AMP_FACTOR
				# max = 1.15 * POS_AMP_FACTOR
				MIN_POS_AMP_FACTOR = 0.85 * POS_AMP_FACTOR
				MAX_POS_AMP_FACTOR = 1.15 * POS_AMP_FACTOR
				POS_AMP_FACTOR = random.choice(numpy.arange(MIN_POS_AMP_FACTOR, MAX_POS_AMP_FACTOR, 0.05))


				max_umis = float(DEPTH)/2/float(POS_AMP_FACTOR)


				while n_umis < max_umis:
					umi = GenerateUMI(UMI_LENGTH)
					
					if umi not in usedUMIS:
						pileup[chrom][pos][pileup[chrom][pos]['ref']][2].append(umi)
						usedUMIS.add(umi)
						n_umis += 1




				read += pileup[chrom][pos]['ref']
				read_qual += quals[int(pileup[chrom][pos]['qScore'])]

				currentPos += 1.0
				



			# umi_pos = 0
			# umi_count = 0
			# for i in range(start, end):
			# 	x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			# 	if x > umi_count:
			# 		umi_pos = i
			# 		umi_count = x
			# 		pos_amp_factor = float(DEPTH)/x

			i = random.choice(range(start, end))
			umi_pos = i
			x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			pos_amp_factor = float(DEPTH)/x


			for umi in pileup[chrom][umi_pos][pileup[chrom][umi_pos]['ref']][2]:
				i = pos_amp_factor

				while i > 0:

					read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 1:N:0:1"
					reads["R1"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual
					i -= 1

					if i > 0:
						read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 2:N:0:1"
						reads["R2"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual

						i -= 1
		
					read_counter += 1




	print("\n")
	PrintTime('console', '\tDone')


	for aRange in reads['R1'].keys():
		keys1 = reads['R1'][aRange]
		keys2 = reads['R2'][aRange]

		if len(keys1) > len(keys2):
			R1 = 'R1'; R2 = 'R2'; i1 = '1'; i2 = '2';
		else:
			R1 = 'R2'; R2 = 'R1'; i1 = '2'; i2 = '1';

		for key in reads[R1][aRange].keys():
			if key.replace(' '+i1+':N:0:1', ' '+i2+':N:0:1') not in reads[R2][aRange].keys():
				del reads[R1][aRange][key]



	return [pileup, reads, usedUMIS]