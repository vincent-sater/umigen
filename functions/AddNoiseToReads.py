#
# THIS FUNCTION ALLOWS TO ADD ARTIFACTS AS NOISE TO THE READS
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -READS             : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS
# 		  -BED               : (STR)  LOCATION OF THE BED FILE
# 		  -READ_LENGTH       : (INT)  LENGTH OF READS
#
# VALUE : -PILEUP            : (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		  -READS             : (DICT) AN UPDATED VERSION OF THE DICTIONNARY WITH ARTIFACTS ADDED TO THE READS

from utils import *
import random

from utils import ParseRanges

def AddNoiseToReads(pileup, reads, ranges_d, BED, READ_LENGTH):

	print("\n")
	PrintTime('console', '\tAdding Noise to Reads...')
	

	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))


	# for each range
	for chrom, ranges in ranges_d.items():
		# get the start and the end
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]

			# build the index
			INDEX = chrom+"-"+str(start)+"-"+str(end)

			# get the reads IDS in R1 and R2
			readsIDS1 = reads['R1'][INDEX].keys()
			readsIDS2 = reads['R2'][INDEX].keys()


			cursor = 0
			# for each position in the read
			for pos in range(start, end+1):

				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				# create a list containing only the non reference bases
				notRef = ['A', 'C', 'G', 'T']
				notRef.remove(pileup[chrom][pos]['ref'])


				# for each non ref base
				for x in notRef:

					# get counts for each sens from the pileup
					count_pos = pileup[chrom][pos][x][0]
					count_neg = pileup[chrom][pos][x][1]

					# keep adding forward reads until counts_pos is reached
					while count_pos > 0:
						
						# raw = the read is not modified at the position
						raw = True

						while raw == True:
							# get a radom read
							chosenKey = random.choice(list(readsIDS1))
							chosenRead = list(reads['R1'][INDEX][chosenKey])
							
							# if the base is UPPERCASE at the position, it means that it wasn't changed so we can modify it and raw becomes False
							# else it means that this read was modifed at this position so we skip and choose another one
							if chosenRead[cursor].isupper():
								chosenRead[cursor] = x.lower()

								reads['R1'][INDEX][chosenKey] = "".join(chosenRead)
								
								raw = False
								count_pos -= 1

										
					# we do the same exact thing for R2 reads
					while count_neg > 0:
						
						raw = True

						while raw == True:
							chosenKey = random.choice(list(readsIDS2))
							chosenRead = list(reads['R2'][INDEX][chosenKey])

							if chosenRead[cursor].isupper():
								chosenRead[cursor] = x.lower()

								reads['R2'][INDEX][chosenKey] = "".join(chosenRead)
								
								raw = False
								count_neg -= 1


				# increment the counters
				cursor += 1
				currentPos += 1.0

		

	print("\n")
	PrintTime('console', '\tDone')

	# return pileup and reads
	return [pileup, reads]