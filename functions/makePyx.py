
import os
import sys
import glob

thisProgramName  = sys.argv[0]

try:
	RAMScriptName   = sys.argv[1]
except:
	RAMScriptName = "RAM.py"

try:
	setupScriptName  = sys.argv[2]
except:
	setupScriptName = "setup.py"


try:
	scripts = glob.glob('*.py')
except:
	pass

try:
	scripts.remove(thisProgramName)
except:
	pass

try:
	scripts.remove(RAMScriptName)
except:
	pass

try:
	scripts.remove(setupScriptName)
except:
	pass

try:
	scripts.remove("TreatReads.py")
except:
	pass






pyxFile = open("functions.pyx", "w")
imports = ""
code = ""


for script in scripts:
	scripFile = open(script)
	for line in scripFile:
		if "import" in line:
			line = line.replace("\t", "")
			if line not in imports and "*" not in line and "#" not in line:
				imports += line.replace("\t", "")
		else:
			line = line.replace('    ', '\t')
			code += line

	code += "\n\n"



pyxFile.write(imports+"\n\n"+code+"\n\n")

print("\nDone\n")