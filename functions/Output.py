#
# THIS FUNCTION ALLOWS TO ADD TRUE VARIANTS TO THE READS
#
# INPUT : -READS             : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS
# 		  -FASTA             : (STR) LOCATION OF THE REFERENCE GENOME FASTA FILE
#         -OUTPUT            : (STR) DIRECTORY IN WHICH THE PRODUCED SAMPLES ARE CREATED
# 		  -OUTPUT_NAME       : (STR) THE NAME OF THE PRODUCED SAMPLE
#  	 	  -INSERTIONS        : (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC INSERTION
#         -DELETIONS         : (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC DELETION
#
# VALUE : 
#		   NONE


import os
from utils import *

def Output(reads, FASTA, output, output_name, insertions, deletions, NAME, PLATFORM, qualityMatrix):

	# print output
	# print output_name


	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ"
	qualsToScores = {}
	scoresToQuals = {}


	i=0
	for q in quals_str:
		qualsToScores[quals_str[i]] = i 
		scoresToQuals[i] = quals_str[i]
		i += 1

	
	AlphaToLen = {
		"del": {"b": 1, "d": 2, "e": 3, "f": 4, "h": 5, "i": 6, "j": 7, "k": 8, "l": 9, "m": 10}, 
		"in" : {"n": 1, "o": 2, "p": 3, "q": 4, "r": 5, "s": 6, "u": 7, "v": 8, "w": 9, "x": 10}
	}


	
	totalPos = float(GetTotalPos2(reads["R1"])*2)
	currentPos = 1.0
	lastProgress = 0.0

	print("\n")
	PrintTime("console", "\tGenerating FASTQ...")

	# => fastq_R1
	output_R1 = open(output+"/"+output_name+"_R1.fastq", 'w')

	for x in reads["R1"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])

		searchForInDel = False
		for del_index in deletions.keys():
			if int(del_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for in_index in insertions.keys():
			if int(in_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for read_id, read in reads["R1"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0]
			eventLen = False
			event = False
			
			if searchForInDel:
				for base in read:
					if base not in 'ACGTacgt':
						base_idx = read.index(base)
						try:
							eventLen = AlphaToLen["del"][base]
							event = "del"
							read = read[0:base_idx]+deletions[del_index][1]+read[base_idx+1+eventLen:]

						except:
							eventLen = AlphaToLen["in"][base]
							event = "in"

							read = read[0:base_idx]+insertions[in_index][0]+insertions[in_index][1]+read[base_idx+1:]


			read_qual = readArray[1]
			if eventLen:
				if event == "in":
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]*(eventLen+1)+read_qual[base_idx+1:]
				else:
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]+read_qual[base_idx+1+eventLen:]


			read = read.upper()


			### add function to simulate loss of quality at end of reads
			read_qual = list(read_qual)
			# print(len(read_qual))
			for pos in range(0, len(read_qual)):
				old_score = qualsToScores[read_qual[pos]]
				new_score = int(round(old_score*qualityMatrix[pos], 0))
				new_qual = scoresToQuals[new_score]
				read_qual[pos] = new_qual

			read_qual = "".join(read_qual)

			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R1.write(line)

			currentPos += 1.0

	output_R1.close()





	# => fastq_R2
	output_R2 = open(output+"/"+output_name+"_R2.fastq", 'w')

	for x in reads["R2"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])

		searchForInDel = False
		for del_index in deletions.keys():
			if int(del_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for in_index in insertions.keys():
			if int(in_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for read_id, read in reads["R2"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0]
			eventLen = False
			event = False
			
			if searchForInDel:
				for base in read:
					if base not in 'ACGTacgt':
						base_idx = read.index(base)
						try:
							eventLen = AlphaToLen["del"][base]
							event = "del"

							read = read[0:base_idx]+deletions[del_index][1]+read[base_idx+1+eventLen:]

						except:
							eventLen = AlphaToLen["in"][base]
							event = "in"

							read = read[0:base_idx]+insertions[in_index][0]+insertions[in_index][1]+read[base_idx+1:]


			read_qual = readArray[1]
			if eventLen:
				if event == "in":
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]*(eventLen+1)+read_qual[base_idx+1:]
				else:
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]+read_qual[base_idx+1+eventLen:]


			read = ReverseComplement(read)
			read = read.upper()
			

			### add function to simulate loss of quality at end of reads
			read_qual = list(read_qual)

			for pos in range(0, len(read_qual)):
				old_score = qualsToScores[read_qual[pos]]
				new_score = int(round(old_score*qualityMatrix[pos], 0))
				new_qual = scoresToQuals[new_score]
				read_qual[pos] = new_qual
					
			read_qual = "".join(read_qual)

			read_qual = read_qual[::-1]

			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R2.write(line)

			currentPos += 1.0

	output_R2.close()






	print("\n")
	PrintTime("console", "\tDone")
	print("\n")


	# bwa => sam
	PrintTime("console", "\tAligning Reads (using BWA)")
	os.system('bwa mem -t 10 -R "@RG\\tID:'+NAME+'\\tPL:'+PLATFORM+'\\tSM:'+NAME+'" '+FASTA+' '+output+"/"+output_name+'_R1.fastq '+output+"/"+output_name+'_R2.fastq > '+output+"/"+output_name+'.sam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => bam
	PrintTime("console", "\tConverting SAM to BAM...")
	os.system('samtools view -Sb '+output+"/"+output_name+'.sam > '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => sorted_bam
	PrintTime("console", "\tSorting BAM...")
	os.system('samtools sort '+output+"/"+output_name+'.bam > '+output+"/"+output_name+'_sorted.bam')
	os.remove(output+"/"+output_name+".bam")
	os.rename(output+"/"+output_name+"_sorted.bam", output+"/"+output_name+".bam")
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => index
	PrintTime("console", "\tIndexing BAM...")
	os.system('samtools index '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')